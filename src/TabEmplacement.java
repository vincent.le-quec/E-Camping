import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class TabEmplacement extends AbstractTableModel {
    public final int ligne = 10;
    public final int colonne = 10;
    public static int tab[][] = new int[10][10];
    private int num = 1;
    private ArrayList<Integer> ls = new ArrayList<Integer>();
    private final String[] entetes = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

    public TabEmplacement() {
        //super(); // utile ???
    }

    public void creerTab() {
        for (int i = 0; i < ligne; i++) {
            for (int j = 0; j < colonne; j++) {
                tab[i][j] = num;
                num++;
            }
        }
    }

    // Debug
    public void affiche() {
        for (int i = 0; i < ligne; i++) {
            for (int j = 0; j < colonne; j++) {
                if (tab[i][j] < 10)
                    System.out.print(' ');
                if (j == 0)
                    System.out.println(' ');
                System.out.print(" " + tab[i][j] + " ");
            }
        }
    }

    public void initEmp_between(int numcase, int newnum) {
        int x = 1;

        for (int i = 0; i < colonne; i++) {
            for (int j = 0; j < ligne; j++) {
                if (tab[i][j] == numcase)
                    tab[i][j] = newnum;
                else if (tab[i][j] >= newnum && tab[i][j] <= numcase)
                    tab[i][j] = 0;
                else if (tab[i][j] > numcase)
                    tab[i][j] = newnum + x++;
            }
        }

    }

    public int getRowCount() {
        return tab.length;
    }

    public int getColumnCount() {
        return entetes.length;
    }

    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        return tab[rowIndex][columnIndex];//donnees[rowIndex][columnIndex];
    }
}

