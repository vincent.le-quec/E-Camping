import java.io.Serializable;

public class Emplacement implements Serializable {
    private static final long serialVersionUID = 21L;

    private String type;
    private int numero;
    private boolean libre;

    public Emplacement(String type, int numero, boolean libre) {
        this.type = type;
        this.numero = numero;
        this.libre = libre;
    }

    public String getType() {
        return type;
    }
    public int getNumero() {
        return numero;
    }
    public boolean getLibre() { return libre; }

    @Override
    public String toString() {
        return "Emplacement{" +
                "type=" + type + '\'' +
                ", numero=" + numero +
                ", libre=" + libre +
                '}';
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Emplacement)) return false;

        Emplacement that = (Emplacement) o;

        return numero == that.numero;
    }
}
