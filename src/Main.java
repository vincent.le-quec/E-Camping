import javafx.application.Application;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

public class Main extends Application {
    private static final String dir = "save";
    private static final String file = "save.camp";
    private static final String path = dir + "/" + file;
    private static File f = null;

    private static My_Date today = new My_Date();
    private static ArrayList<Reservation> list = new ArrayList<Reservation>();

    // files
    public static int check_d() {
        File d = new File(dir);

        if (!d.isDirectory())
            if (!d.mkdir()) {
                System.out.println("[Erreur] Impossible de créer le dossier " + dir + ".");
                return -1;
            }
        return 0;
    }

    public static int load() throws IOException {
        ObjectInputStream in;
        FileInputStream fis;

        try {
            fis = new FileInputStream(path);
            in = new ObjectInputStream(fis);
            while (fis.available() > 0)
                list.add((Reservation) in.readObject());
            in.close();
        } catch (FileNotFoundException e) {
            // /!\ PAS DE LOAD == GENERATION ?? /!\
            System.out.println("[Attention] Aucun fichier de sauvegarde.\nGénération en cours.");
            // generation
        } catch (Exception e) { // pour readObject
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    public static int save() throws IOException {
        ObjectOutputStream out;

        try {
            out = new ObjectOutputStream(new FileOutputStream(path));
            for (int i = 0; i < list.size(); i++)
                out.writeObject(list.get(i));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    // Reservations
    public static boolean dispo(int day, int month, int year, Personne p, Emplacement emp) {
        My_Date tmp = new My_Date(day, month, year);
        for (int i = 0; i < list.size(); i++)
            if (list.get(i).getEmp().equals(emp) && list.get(i).getD().isBefore(tmp) && tmp.isBefore(list.get(i).getEnd()))
                return false;
        return true;
    }

    public static int addReservation(int day, int month, int year, Personne p, int nb, int nb_j, Emplacement emp) {
        if (list.isEmpty())
            list.add(new Reservation(day, month, year, p, nb, nb_j, emp));
        else {
            if (today.isBefore(new My_Date(day, month, year)) && dispo(day, month, year, p, emp))
                list.add(new Reservation(day, month, year, p, nb, nb_j, emp));
            else
                return -1;
        }
        return 0;
    }

    public static Reservation find_res(String nom, String prenom) {
        for (int i = 0; i < list.size(); i++)
            if (list.get(i).getP().getNom().equals(nom) && list.get(i).getP().getPrenom().equals(prenom))
                return list.get(i);
        return null;
    }

    public static ArrayList<Reservation> getList(){
        return list;
    }


    // interface
    public void start(Stage primaryStage) throws IOException {
        primaryStage = new Interface();
        primaryStage.show();
    }

    public static void main(String args[]) throws IOException {
        // Load file
        if (check_d() == 0)
            load();

        // Start interface
        Application.launch(args);

//        Personne p1 = new Personne("test1", "test1", "adr", "0123456789");
//        Emplacement e1 = new Emplacement("Bungalow", 10);
//        Reservation r1 = new Reservation(1, 8, 2017, p1, 2, 42, e1);
//        list.add(r1);
//
//        Personne p2 = new Personne("test2", "test2", "adr", "0123456789");
//        Emplacement e2 = new Emplacement("Camping-car", 9);
//        Reservation r2 = new Reservation(1, 8, 2017, p2, 2, 42, e2);
//        list.add(r2);
//
//        save();
//        System.out.println("end");
    }
}
