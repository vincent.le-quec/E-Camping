import javax.swing.*;
import java.awt.*;

/**
 * Created by Quentin on 15/04/2017.
 */
public class Afftab extends JFrame {

    public Afftab() {

        setTitle("JTable avec modèle statique");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JTable tableau = new JTable(new TabEmplacement());

        getContentPane().add(tableau.getTableHeader(), BorderLayout.NORTH);
        getContentPane().add(tableau, BorderLayout.CENTER);

        pack();
        tableau.setDefaultRenderer(Object.class, new ColorTab()); //mettre Integer.class a la place de Object
    }
}

