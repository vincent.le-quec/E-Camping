import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.util.StringTokenizer;


public class Interface extends Stage {
    private Image image = new Image("img/logo.png");
    private ImageView iv1 = new ImageView();
    private Region fond = new Region();
    private StackPane entete = new StackPane();
    private ObservableList liste = entete.getChildren();
    private DatePicker date = new DatePicker();
    private Line ligne = new Line(0, 0, 10000, 0);
    private Line ligne2 = new Line(0, 0, 10000, 0);
    private Label msgInfo = new Label();
    private Label msgInfomod = new Label();
    private Label demande = new Label("Quelle date recherchez-vous ?");
    private Label nom = new Label("Nom :");
    private Label prenom = new Label("Prenom :");
    private Label telephone = new Label("Téléphone :");
    private Label adresse = new Label("Adresse :");
    private Label nombre = new Label("Nombre de personne(s):");
    private Label jour = new Label("Nombre de jour(s) :");
    private Label emplacement = new Label("Emplacement :");
    private Label nommod = new Label("Nom :");
    private Label prenommod = new Label("Prenom :");
    private Label telephonemod = new Label("Téléphone :");
    private Label adressemod = new Label("Adresse :");
    private Label nombremod = new Label("Nombre de personne(s):");
    private Label jourmod = new Label("Nombre de jour(s) :");
    private Label emplacementmod = new Label("Emplacement :");
    private Label msgInfosup = new Label();
    private Label nomsup = new Label("Nom :");
    private Label prenomsup = new Label("Prenom :");
    private Label telephonesup = new Label("Téléphone :");
    private Label adressesup = new Label("Adresse :");
    private Label nombresup = new Label("Nombre de personne(s):");
    private Label joursup = new Label("Nombre de jour(s) :");
    private Label emplacementsup = new Label("Emplacement :");
    private TextField txtnom = new TextField();
    private TextField txtprenom = new TextField();
    private TextField txttelephone = new TextField();
    private TextField txtadresse = new TextField();
    private TextField txtnombre = new TextField();
    private TextField txtjour = new TextField();
    private TextField txtemplacement = new TextField();
    private TextField prix = new TextField();
    private TextField txtnommod = new TextField();
    private TextField txtprenommod = new TextField();
    private TextField txttelephonemod = new TextField();
    private TextField txtadressemod = new TextField();
    private TextField txtnombremod = new TextField();
    private TextField txtjourmod = new TextField();
    private TextField txtemplacementmod = new TextField();
    private TextField txtnomsup = new TextField();
    private TextField txtprenomsup = new TextField();
    private TextField txttelephonesup = new TextField();
    private TextField txtadressesup = new TextField();
    private TextField txtnombresup = new TextField();
    private TextField txtjoursup = new TextField();
    private TextField txtemplacementsup = new TextField();
    private TextField recherchenom = new TextField();
    private TextField rechercheprenom = new TextField();
    private TextField recherchesupnom = new TextField();
    private TextField recherchesupprenom = new TextField();
    private Button valider = new Button("Chercher");
    private Button calculer = new Button("Calculer");
    private Button btValider = new Button("Enregistrer");
    private Button btAnnuler = new Button("Annuler");
    private Button valide = new Button("Chercher");
    private Button btValidermod = new Button("Enregistrer");
    private Button btAnnulermod = new Button("Annuler");
    private Button validesup = new Button("Chercher");
    private Button btSupprimer = new Button("Supprimer");
    private HBox choix = new HBox(demande, date, valider);
    private HBox inputNom = new HBox(nom, txtnom);
    private HBox inputPrenom = new HBox(prenom, txtprenom);
    private HBox inputTelephone = new HBox(telephone, txttelephone);
    private HBox inputAdresse = new HBox(adresse, txtadresse);
    private HBox inputNombre = new HBox(nombre, txtnombre);
    private HBox inputJour = new HBox(jour, txtjour);
    private HBox inputEmplacement = new HBox(emplacement, txtemplacement);
    private HBox inputNommod = new HBox(nommod, txtnommod);
    private HBox inputPrenommod = new HBox(prenommod, txtprenommod);
    private HBox inputTelephonemod = new HBox(telephonemod, txttelephonemod);
    private HBox inputAdressemod = new HBox(adressemod, txtadressemod);
    private HBox inputNombremod = new HBox(nombremod, txtnombremod);
    private HBox inputJourmod = new HBox(jourmod, txtjourmod);
    private HBox inputEmplacementmod = new HBox(emplacementmod, txtemplacementmod);
    private HBox inputNomsup = new HBox(nomsup, txtnomsup);
    private HBox inputPrenomsup = new HBox(prenomsup, txtprenomsup);
    private HBox inputTelephonesup = new HBox(telephonesup, txttelephonesup);
    private HBox inputAdressesup = new HBox(adressesup, txtadressesup);
    private HBox inputNombresup = new HBox(nombresup, txtnombresup);
    private HBox inputJoursup = new HBox(joursup, txtjoursup);
    private HBox inputEmplacementsup = new HBox(emplacementsup, txtemplacementsup);
    private HBox zoneRecherche = new HBox(recherchenom, rechercheprenom, valide);
    private HBox zoneRecherchesup = new HBox(recherchesupnom, recherchesupprenom, validesup);
    private HBox boutonsmod = new HBox(btValidermod, btAnnulermod);
    private HBox boutons = new HBox(btValider, btAnnuler);
    private Button btAnnulersup = new Button("Annuler");
    private HBox boutonssup = new HBox(btSupprimer, btAnnulersup);
    private VBox inputgeneral = new VBox(inputNom, inputPrenom, inputTelephone, inputAdresse, inputNombre, inputJour, inputEmplacement);
    private VBox inputgeneralmod = new VBox(inputNommod, inputPrenommod, inputTelephonemod, inputAdressemod, inputNombremod, inputJourmod, inputEmplacementmod);
    private VBox inputgeneralsup = new VBox(inputNomsup, inputPrenomsup, inputTelephonesup, inputAdressesup, inputNombresup, inputJoursup, inputEmplacementsup);
    private GridPane rootemp = new GridPane();
    private GridPane rootres = new GridPane();
    private GridPane rootmod = new GridPane();
    private GridPane rootsup = new GridPane();
    private TabPane tp = new TabPane();
    private Tab emp = new Tab("Emplacement");
    private Tab res = new Tab("Reservation");
    private Tab mod = new Tab("Modification");
    private Tab sup = new Tab("Supprimer");
    private VBox fenetre = new VBox();
    private Scene ecran = new Scene(contenu());


    private static int day;
    private static int month;
    private static int year;
    private static Emplacement tmpemp;
    private static Reservation tmpres;

    private final int MAX_EMP = 25;

    public Interface() {
        this.setTitle("E-Camping - Projet S2");
        this.setMaximized(true);
        ecran.getStylesheets().add("style.css");
        this.setScene(ecran);
        //this.setMinHeight(300);
        //this.setMinWidth(200);
    }

    public Parent contenu(){

        iv1.setImage(image);
        iv1.setFitWidth(300);
        iv1.setPreserveRatio(true);
        iv1.setSmooth(true);

        fond.setId("region");
        fond.setPrefSize(100, 100);

        liste.addAll(fond, iv1);
        StackPane.setAlignment(iv1, Pos.CENTER_LEFT);

        date.setPromptText(new My_Date().toString());

        //Interface Emplacement
        HBox.setMargin(demande, new Insets(0, 10, 5, 0));
        HBox.setMargin(date, new Insets(0, 10, 5, 10));
        HBox.setMargin(valider, new Insets(0, 0, 5, 0));

        rootemp.setHgap(6);
        rootemp.setVgap(6);

        Button numEmp[] = new Button[MAX_EMP];
        GridPane tableauEmp = new GridPane();

        choix.setAlignment(Pos.CENTER);
        tableauEmp.setAlignment(Pos.CENTER);

        rootemp.add(choix, 0, 0, 4, 1);


        GridPane.setHalignment(choix, HPos.CENTER);
        GridPane.setValignment(choix, VPos.CENTER);


        rootemp.getColumnConstraints().setAll(
                new ColumnConstraints(90, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(90, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(500, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(520, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));

        rootemp.getColumnConstraints().get(0).setHgrow(Priority.ALWAYS);
        rootemp.getColumnConstraints().get(1).setHgrow(Priority.ALWAYS);
        rootemp.getColumnConstraints().get(2).setHgrow(Priority.ALWAYS);
        rootemp.getColumnConstraints().get(3).setHgrow(Priority.ALWAYS);

        rootemp.getRowConstraints().setAll(
                new RowConstraints(75, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(400, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootemp.getRowConstraints().get(0).setVgrow(Priority.NEVER);
        rootemp.getRowConstraints().get(1).setVgrow(Priority.ALWAYS);


        //Interface Reservation
        txtnom.setMaxWidth(220);
        txtnom.setPromptText("Nom");
        txtprenom.setMaxWidth(220);
        txtprenom.setPromptText("Prénom");
        txttelephone.setMaxWidth(220);
        txttelephone.setPromptText("00 00 00 00 00");
        txtadresse.setMaxWidth(220);
        txtadresse.setPromptText("Adresse");
        txtnombre.setMaxWidth(220);
        txtnombre.setPromptText("Nombre");
        txtjour.setMaxWidth(220);
        txtjour.setPromptText("Jours");
        txtemplacement.setMaxWidth(220);
        txtemplacement.setEditable(false);
        txtemplacement.setPromptText("Pré-rempli");
        txtemplacement.setId("muted");
        prix.setMaxWidth(220);
        prix.setPromptText("Prix : ");
        prix.setEditable(false);

        HBox.setMargin(nom, new Insets(0, 220, 0, 50));
        HBox.setMargin(prenom, new Insets(0, 200, 0, 50));
        HBox.setMargin(telephone, new Insets(0, 182, 0, 50));
        HBox.setMargin(adresse, new Insets(0, 198, 0, 50));
        HBox.setMargin(nombre, new Insets(0, 100, 0, 50));
        HBox.setMargin(jour, new Insets(0, 132, 0, 50));
        HBox.setMargin(emplacement, new Insets(0, 161, 0, 50));

        VBox.setMargin(inputNom, new Insets(15));
        VBox.setMargin(inputPrenom, new Insets(15));
        VBox.setMargin(inputTelephone, new Insets(15));
        VBox.setMargin(inputAdresse, new Insets(15));
        VBox.setMargin(inputNombre, new Insets(15));
        VBox.setMargin(inputJour, new Insets(15));
        VBox.setMargin(inputEmplacement, new Insets(15));

        for (int i=0; i<numEmp.length; i++) {
            numEmp[i] = new Button(Integer.toString(i));
            numEmp[i].setMinSize(70, 70);
            tableauEmp.add(numEmp[i], i % 5, i / 5);
            numEmp[i].setCursor(Cursor.OPEN_HAND);
            numEmp[i].setDisable(false);
        }

        rootemp.add(tableauEmp, 0, 1, 4, 1);

        calculer.setDisable(true);
        VBox.setMargin(calculer, new Insets(15));
        VBox calculPrix = new VBox(prix, calculer);

        btValider.setDisable(true);


        HBox.setMargin(btValider, new Insets(10));
        HBox.setMargin(btAnnuler, new Insets(10));

        rootres.setHgap(6);
        rootres.setVgap(6);


        inputgeneral.setAlignment(Pos.CENTER);
        boutons.setAlignment(Pos.TOP_CENTER);
        calculPrix.setAlignment(Pos.CENTER);
        msgInfo.setAlignment(Pos.CENTER);

        rootres.add(msgInfo, 2, 0, 1, 1);
        rootres.add(inputgeneral, 2, 1, 1, 1);
        rootres.add(boutons, 2, 3, 1, 1);
        rootres.add(calculPrix, 3, 1, 1, 1);


        rootres.getColumnConstraints().setAll(
                new ColumnConstraints(150, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(150, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(500, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(520, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootres.getColumnConstraints().get(0).setHgrow(Priority.ALWAYS);
        rootres.getColumnConstraints().get(1).setHgrow(Priority.ALWAYS);
        rootres.getColumnConstraints().get(2).setHgrow(Priority.ALWAYS);
        rootres.getColumnConstraints().get(3).setHgrow(Priority.ALWAYS);

        rootres.getRowConstraints().setAll(
                new RowConstraints(75, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(50, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootres.getRowConstraints().get(0).setVgrow(Priority.NEVER);
        rootres.getRowConstraints().get(1).setVgrow(Priority.NEVER);
        rootres.getRowConstraints().get(2).setVgrow(Priority.NEVER);


        //Interface Modification
        recherchenom.setPromptText("Nom");
        rechercheprenom.setPromptText("Prénom");

        HBox.setMargin(valide, new Insets(0, 0, 5, 0));
        HBox.setMargin(recherchenom, new Insets(0, 0, 5, 0));
        HBox.setMargin(rechercheprenom, new Insets(0, 0, 5, 0));

        txtnommod.setMaxWidth(220);
        txtnommod.setPromptText("Nom");
        txtnommod.setEditable(false);
        txtnommod.setId("muted");
        txtprenommod.setMaxWidth(220);
        txtprenommod.setPromptText("Prénom");
        txtprenommod.setEditable(false);
        txtprenommod.setId("muted");
        txttelephonemod.setMaxWidth(220);
        txttelephonemod.setPromptText("00 00 00 00 00");
        txtadressemod.setMaxWidth(220);
        txtadressemod.setPromptText("Adresse");
        txtnombremod.setMaxWidth(220);
        txtnombremod.setPromptText("Nombre");
        txtjourmod.setMaxWidth(220);
        txtjourmod.setPromptText("Jours");
        txtemplacementmod.setMaxWidth(220);
        txtemplacementmod.setEditable(false);
        txtemplacementmod.setPromptText("Pré-rempli");
        txtemplacementmod.setId("muted");

        HBox.setMargin(nommod, new Insets(0, 220, 0, 50));
        HBox.setMargin(prenommod, new Insets(0, 200, 0, 50));
        HBox.setMargin(telephonemod, new Insets(0, 182, 0, 50));
        HBox.setMargin(adressemod, new Insets(0, 198, 0, 50));
        HBox.setMargin(nombremod, new Insets(0, 100, 0, 50));
        HBox.setMargin(jourmod, new Insets(0, 132, 0, 50));
        HBox.setMargin(emplacementmod, new Insets(0, 161, 0, 50));

        VBox.setMargin(inputNommod, new Insets(15));
        VBox.setMargin(inputPrenommod, new Insets(15));
        VBox.setMargin(inputTelephonemod, new Insets(15));
        VBox.setMargin(inputAdressemod, new Insets(15));
        VBox.setMargin(inputNombremod, new Insets(15));
        VBox.setMargin(inputJourmod, new Insets(15));
        VBox.setMargin(inputEmplacementmod, new Insets(15));

        btValidermod.setDisable(true);
        HBox.setMargin(btValidermod, new Insets(10));
        HBox.setMargin(btAnnulermod, new Insets(10));

        rootmod.setHgap(6);
        rootmod.setVgap(6);


        GridPane.setValignment(ligne2, VPos.BOTTOM);
        inputgeneralmod.setAlignment(Pos.CENTER);
        boutonsmod.setAlignment(Pos.TOP_CENTER);
        zoneRecherche.setAlignment(Pos.CENTER);
        msgInfomod.setAlignment(Pos.CENTER);

        rootmod.add(msgInfomod, 2, 1, 1, 1);
        rootmod.add(zoneRecherche, 2, 0, 1, 1);
        rootmod.add(ligne2, 0, 1, Integer.MAX_VALUE, 1);
        rootmod.add(inputgeneralmod, 2, 2, 1, 1);
        rootmod.add(boutonsmod, 2, 3, 1, 1);


        rootmod.getColumnConstraints().setAll(
                new ColumnConstraints(150, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(150, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(500, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(520, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootmod.getColumnConstraints().get(0).setHgrow(Priority.ALWAYS);
        rootmod.getColumnConstraints().get(1).setHgrow(Priority.ALWAYS);
        rootmod.getColumnConstraints().get(2).setHgrow(Priority.ALWAYS);
        rootmod.getColumnConstraints().get(3).setHgrow(Priority.ALWAYS);

        rootmod.getRowConstraints().setAll(
                new RowConstraints(75, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootmod.getRowConstraints().get(0).setVgrow(Priority.NEVER);
        rootmod.getRowConstraints().get(1).setVgrow(Priority.NEVER);
        rootmod.getRowConstraints().get(2).setVgrow(Priority.NEVER);
        rootmod.getRowConstraints().get(3).setVgrow(Priority.ALWAYS);
        rootmod.getRowConstraints().get(4).setVgrow(Priority.ALWAYS);


        //Interface Supprimer
        recherchesupnom.setPromptText("Nom");
        recherchesupprenom.setPromptText("Prénom");

        HBox.setMargin(validesup, new Insets(0, 0, 5, 0));
        HBox.setMargin(recherchesupnom, new Insets(0, 0, 5, 0));
        HBox.setMargin(recherchesupprenom, new Insets(0, 0, 5, 0));

        txtnomsup.setMaxWidth(220);
        txtnomsup.setPromptText("Nom");
        txtnomsup.setEditable(false);
        txtprenomsup.setMaxWidth(220);
        txtprenomsup.setPromptText("Prénom");
        txtprenomsup.setEditable(false);
        txttelephonesup.setMaxWidth(220);
        txttelephonesup.setPromptText("00 00 00 00 00");
        txttelephonesup.setEditable(false);
        txtadressesup.setMaxWidth(220);
        txtadressesup.setPromptText("Adresse");
        txtadressesup.setEditable(false);
        txtnombresup.setMaxWidth(220);
        txtnombresup.setPromptText("Nombre");
        txtnombresup.setEditable(false);
        txtjoursup.setMaxWidth(220);
        txtjoursup.setPromptText("Jours");
        txtjoursup.setEditable(false);
        txtemplacementsup.setMaxWidth(220);
        txtemplacementsup.setEditable(false);
        txtemplacementsup.setPromptText("Pré-rempli");

        HBox.setMargin(nomsup, new Insets(0, 220, 0, 50));
        HBox.setMargin(prenomsup, new Insets(0, 200, 0, 50));
        HBox.setMargin(telephonesup, new Insets(0, 182, 0, 50));
        HBox.setMargin(adressesup, new Insets(0, 198, 0, 50));
        HBox.setMargin(nombresup, new Insets(0, 100, 0, 50));
        HBox.setMargin(joursup, new Insets(0, 132, 0, 50));
        HBox.setMargin(emplacementsup, new Insets(0, 161, 0, 50));

        VBox.setMargin(inputNomsup, new Insets(15));
        VBox.setMargin(inputPrenomsup, new Insets(15));
        VBox.setMargin(inputTelephonesup, new Insets(15));
        VBox.setMargin(inputAdressesup, new Insets(15));
        VBox.setMargin(inputNombresup, new Insets(15));
        VBox.setMargin(inputJoursup, new Insets(15));
        VBox.setMargin(inputEmplacementsup, new Insets(15));

        btSupprimer.setDisable(true);

        HBox.setMargin(btSupprimer, new Insets(10));
        HBox.setMargin(btAnnulersup, new Insets(10));

        rootsup.setHgap(6);
        rootsup.setVgap(6);


        GridPane.setValignment(ligne, VPos.BOTTOM);
        inputgeneralsup.setAlignment(Pos.CENTER);
        boutonssup.setAlignment(Pos.TOP_CENTER);
        zoneRecherchesup.setAlignment(Pos.CENTER);
        msgInfosup.setAlignment(Pos.CENTER);

        rootsup.add(msgInfosup, 2, 1, 1, 1);
        rootsup.add(zoneRecherchesup, 2, 0, 1, 1);
        rootsup.add(ligne, 0, 1, Integer.MAX_VALUE, 1);
        rootsup.add(inputgeneralsup, 2, 2, 1, 1);
        rootsup.add(boutonssup, 2, 3, 1, 1);

        rootsup.getColumnConstraints().setAll(
                new ColumnConstraints(150, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(150, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(500, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new ColumnConstraints(520, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootsup.getColumnConstraints().get(0).setHgrow(Priority.ALWAYS);
        rootsup.getColumnConstraints().get(1).setHgrow(Priority.ALWAYS);
        rootsup.getColumnConstraints().get(2).setHgrow(Priority.ALWAYS);
        rootsup.getColumnConstraints().get(3).setHgrow(Priority.ALWAYS);

        rootsup.getRowConstraints().setAll(
                new RowConstraints(75, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE),
                new RowConstraints(25, Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE));
        rootsup.getRowConstraints().get(0).setVgrow(Priority.NEVER);
        rootsup.getRowConstraints().get(1).setVgrow(Priority.NEVER);
        rootsup.getRowConstraints().get(2).setVgrow(Priority.NEVER);
        rootsup.getRowConstraints().get(3).setVgrow(Priority.ALWAYS);
        rootsup.getRowConstraints().get(4).setVgrow(Priority.ALWAYS);



        emp.setContent(rootemp);
        emp.setClosable(false);
        res.setContent(rootres);
        res.setClosable(false);
        mod.setContent(rootmod);
        mod.setClosable(false);
        sup.setContent(rootsup);
        sup.setClosable(false);

        tp.getTabs().addAll(emp, res, mod, sup);

        fenetre.getChildren().addAll(entete, tp);
        fenetre.setId("windows");


        //Design
        btValider.setCursor(Cursor.OPEN_HAND);
        btAnnuler.setCursor(Cursor.OPEN_HAND);
        btAnnulermod.setCursor(Cursor.OPEN_HAND);
        btAnnulersup.setCursor(Cursor.OPEN_HAND);
        btSupprimer.setCursor(Cursor.OPEN_HAND);
        btValidermod.setCursor(Cursor.OPEN_HAND);
        calculer.setCursor(Cursor.OPEN_HAND);
        valide.setCursor(Cursor.OPEN_HAND);
        valider.setCursor(Cursor.OPEN_HAND);
        validesup.setCursor(Cursor.OPEN_HAND);

        btValider.setId("btn_green");
        btValidermod.setId("btn_green");
        btSupprimer.setId("btn_green");
        btAnnuler.setId("btn_red");
        btAnnulermod.setId("btn_red");
        btAnnulersup.setId("btn_red");
        calculer.setId("btn_blue");
        valide.setId("btn_blue");
        valider.setId("btn_blue");
        validesup.setId("btn_blue");
        msgInfosup.setId("erreur");
        msgInfomod.setId("erreur");
        msgInfo.setId("erreur");

        /** Boutons **/
        // Tab emp
        valider.setOnAction(e -> {
            LocalDate localDate = date.getValue();
            if (localDate != null) {
                for (int i = 0; i < numEmp.length; i++) {
                    numEmp[i].setId("emp_free");
                    numEmp[i].setDisable(false);
                }
                day = localDate.getDayOfMonth();
                month = localDate.getMonthValue();
                year = localDate.getYear();
                My_Date tmp = new My_Date(day, month, year);
                for (int i = 0; i < Main.getList().size(); i++) {
                    if (Main.getList().get(i).getD().isBefore(tmp)
                            && !Main.getList().get(i).getEnd().isBefore(tmp)) {
                        numEmp[Main.getList().get(i).getEmp().getNumero()].setId("emp_use");
                        numEmp[Main.getList().get(i).getEmp().getNumero()].setDisable(true);
                    }
                }
            }
        });
        for (int i = 0; i < numEmp.length; i++) {
            int vali = i;
            numEmp[i].setOnAction(e -> {
                msgInfo.setText("");
                LocalDate localDate = date.getValue();
                if (localDate != null) {
                    day = localDate.getDayOfMonth();
                    month = localDate.getMonthValue();
                    year = localDate.getYear();
                    My_Date tmp = new My_Date(day, month, year);
                    for (int j = 0; j < Main.getList().size(); j++) {
                        if (Main.getList().get(j).getD().isBefore(tmp)
                                && !Main.getList().get(j).getEnd().isBefore(tmp)
                                && Main.getList().get(j).getEmp().getNumero() == vali) {
                            txtemplacement.setText((vali) + " - " + Main.getList().get(j).getEmp().getType());
                        } else if (vali < 10) {
                            txtemplacement.setText("Bungalow - " + vali);
                        } else if (vali < 20) {
                            txtemplacement.setText("Tente - " + vali);
                        } else {
                            txtemplacement.setText("Camping-Car - " + vali);
                        }
                    }
                }
                tp.getSelectionModel().select(res);
            });
        }

        // Add res
        inputgeneral.setOnKeyReleased(e -> {
            if (!txtnom.getText().isEmpty() &&
                    !txtprenom.getText().isEmpty() &&
                    !txttelephone.getText().isEmpty() &&
                    !txtadresse.getText().isEmpty() &&
                    !txtnombre.getText().isEmpty() &&
                    !txtjour.getText().isEmpty()) {
                if (!isNumeric(txtnombre.getText()))
                    setInfo(msgInfo, "Le nombre de personne n'est pas valide.", Color.RED);
                else if (!isNumeric(txtjour.getText()))
                    setInfo(msgInfo, "Le nombre de jour n'est pas valide.", Color.RED);
                else {
                    calculer.setDisable(false);
                    btValider.setDisable(false);
                    Personne p = new Personne(txtnom.getText(), txtprenom.getText(), txttelephone.getText(), txtadresse.getText());
                    StringTokenizer str = new StringTokenizer(txtemplacement.getText());
                    String type = str.nextToken();
                    str.nextToken();
                    int num = Integer.parseInt(str.nextToken());
                    tmpemp = new Emplacement(type, num, false);
                    tmpres = new Reservation(day, month, year, p, Integer.parseInt(txtnombre.getText()), Integer.parseInt(txtjour.getText()), tmpemp);
                }
            }
        });
        btValider.setOnAction(e -> {
            msgInfo.setText("");
            Personne p = new Personne(txtnom.getText(), txtprenom.getText(), txttelephone.getText(), txtadresse.getText());
            if (Main.addReservation(day, month, year, p, Integer.parseInt(txtnombre.getText()), Integer.parseInt(txtjour.getText()), tmpemp) == -1)
                setInfo(msgInfo, "La réservation n'est pas conforme.", Color.RED);
            else {
                setInfo(msgInfo, "Réservation ajoutée", Color.GREEN);
                txtnom.clear();
                txtprenom.clear();
                txttelephone.clear();
                txtadresse.clear();
                txtnombre.clear();
                txtjour.clear();
                txtemplacement.clear();
                prix.clear();
                try {
                    Main.save();
//                    Thread.sleep(2000);
//                    setInfo(msgInfo, "Sauvegarde réussie", Color.GREEN);
                } catch (Exception ex) {
                    setInfo(msgInfo, "Echec de la sauvegarde", Color.RED);
                }
            }
        });
        calculer.setOnAction(e -> {
            prix.setText("Prix : " + tmpres.calPrix() + " €");
        });
        btAnnuler.setOnAction(e -> {
            tp.getSelectionModel().select(emp);
            txtnom.clear();
            txtprenom.clear();
            txttelephone.clear();
            txtadresse.clear();
            txtnombre.clear();
            txtjour.clear();
            txtemplacement.clear();
            prix.clear();
        });

        // modif
        valide.setOnAction(e -> {
            if (!recherchenom.getText().isEmpty() && !rechercheprenom.getText().isEmpty()) {
                if ((tmpres = Main.find_res(recherchenom.getText(), rechercheprenom.getText())) != null) {
                    setInfo(msgInfomod, "", Color.RED);
                    txtnommod.setText(tmpres.getP().getNom());
                    txtprenommod.setText(tmpres.getP().getPrenom());
                    txttelephonemod.setText(tmpres.getP().getTelephone());
                    txtadressemod.setText(tmpres.getP().getAdresse());
                    txtnombremod.setText(String.valueOf(tmpres.getNb()));
                    txtjourmod.setText(String.valueOf(tmpres.getNb_j()));
                    txtemplacementmod.setText(tmpres.getEmp().getNumero() + " - " + tmpres.getEmp().getType());
                    btValidermod.setDisable(false);
                } else {
                    setInfo(msgInfomod, "La réservation n'existe pas.", Color.RED);
                }
            } else {
                setInfo(msgInfomod, "Champs vides, merci de rentrer un nom et un prénom.", Color.RED);
            }
        });
        btValidermod.setOnAction(e -> {
            tmpres.getP().setTelephone(txttelephonemod.getText());
            tmpres.getP().setAdresse(txtadressemod.getText());
            tmpres.setNb(Integer.parseInt(txtnombremod.getText()));
            tmpres.setNb_j(Integer.parseInt(txtjourmod.getText()));
            recherchenom.clear();
            rechercheprenom.clear();
            txtnommod.clear();
            txtprenommod.clear();
            txttelephonemod.clear();
            txtadressemod.clear();
            txtnombremod.clear();
            txtjourmod.clear();
            txtemplacementmod.clear();
            setInfo(msgInfomod, "Modification réussie.", Color.GREEN);
            try {
                Main.save();
//                Thread.sleep(2000);
//                setInfo(msgInfomod, "Sauvegarde réussie", Color.GREEN);
            } catch (Exception ex) {
                setInfo(msgInfomod, "Echec de la sauvegarde", Color.RED);
            }
        });
        btAnnulermod.setOnAction(e -> {
            recherchenom.clear();
            rechercheprenom.clear();
            txtnommod.clear();
            txtprenommod.clear();
            txttelephonemod.clear();
            txtadressemod.clear();
            txtnombremod.clear();
            txtjourmod.clear();
            txtemplacementmod.clear();
            setInfo(msgInfomod, "", Color.GREEN);
//            tp.getSelectionModel().select(emp);
        });

        // del
        validesup.setOnAction(e -> {
            if (!recherchesupnom.getText().isEmpty() && !recherchesupprenom.getText().isEmpty()) {
                if ((tmpres = Main.find_res(recherchesupnom.getText(), recherchesupprenom.getText())) != null) {
                    setInfo(msgInfosup, "", Color.RED);
                    txtnomsup.setText(tmpres.getP().getNom());
                    txtprenomsup.setText(tmpres.getP().getPrenom());
                    txttelephonesup.setText(tmpres.getP().getTelephone());
                    txtadressesup.setText(tmpres.getP().getAdresse());
                    txtnombresup.setText(String.valueOf(tmpres.getNb()));
                    txtjoursup.setText(String.valueOf(tmpres.getNb_j()));
                    txtemplacementsup.setText(tmpres.getEmp().getNumero() + " - " + tmpres.getEmp().getType());
                    btSupprimer.setDisable(false);
                } else {
                    setInfo(msgInfosup, "La réservation n'existe pas.", Color.RED);
                }
            } else {
                setInfo(msgInfosup, "Champs vides, merci de rentrer un nom et un prénom.", Color.RED);
            }
        });
        btSupprimer.setOnAction(e -> {
            setInfo(msgInfosup, "", Color.RED);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Êtes-vous sûr ?");
            alert.setHeaderText("Êtes-vous sûr de vouloir supprimer ce client ?");
            alert.setContentText("Ce choix sera définitif...");
            alert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    Main.getList().remove(tmpres);
                    setInfo(msgInfosup, "Suppression réussie.", Color.GREEN);
                    try {
                        Main.save();
//                        Thread.sleep(2000);
//                        setInfo(msgInfosup, "Sauvegarde réussie", Color.GREEN);
                    } catch (Exception ex) {
                        setInfo(msgInfosup, "Echec de la sauvegarde", Color.RED);
                    }
                }
            });

            recherchesupnom.clear();
            recherchesupprenom.clear();
            txtnomsup.clear();
            txtprenomsup.clear();
            txttelephonesup.clear();
            txtadressesup.clear();
            txtnombresup.clear();
            txtjoursup.clear();
            txtemplacementsup.clear();
        });
        btAnnulersup.setOnAction(e -> {
            recherchesupnom.clear();
            recherchesupprenom.clear();
            txtnomsup.clear();
            txtprenomsup.clear();
            txttelephonesup.clear();
            txtadressesup.clear();
            txtnombresup.clear();
            txtjoursup.clear();
            txtemplacementsup.clear();
            setInfo(msgInfosup, "", Color.RED);
//            tp.getSelectionModel().select(emp);
        });

        return (fenetre);
    }

    private static Label setInfo(Label lab, String msg, Color col) {
        lab.setText(msg);
        lab.setTextFill(col);
        return lab;
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
