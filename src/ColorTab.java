import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by Quentin on 15/04/2017.
 */
public class ColorTab extends DefaultTableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (TestReservColor.MY_PUBLIC_LIST.contains(value))
            cell.setBackground(Color.RED);
//        else if (TabEmplacement.equalsZero(value)) {
        else if ((int)value == 0) {
            JLabel label = (JLabel) cell;
            cell.setBackground(Color.pink); // réussir a laisser les chiffres en visu
            //label.setText(label.getText());
        } else
            cell.setBackground(Color.green);
        //Color color = (Color) value;

        setText("");

        return this;
    }

}
