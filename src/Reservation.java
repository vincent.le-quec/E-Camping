import java.io.Serializable;
import java.util.Calendar;

public class Reservation implements Serializable {
    private static final long serialVersionUID = 42L;

    private static double prix_u_bung_hiver = 20.0;
    private static double prix_u_bung_ete = 30.0;

    private static double prix_u_autre_hiver = 10.0;
    private static double prix_u_autre_ete = 15.0;

    private static double prix_p_hiver = 4.0;
    private static double prix_p_ete = 6.0;

    private Personne p;
    private My_Date d;
    private int nb;
    private int nb_j;
    private Emplacement emp;

    private My_Date end;

    public Reservation(int day, int month, int year, Personne p, int nb, int nb_j, Emplacement emp) {
        this.d = new My_Date(day, month, year);
        this.p = p;
        this.nb = nb;
        this.nb_j = nb_j;
        this.emp = emp;

        this.end = new My_Date(day, month, year);
        this.end.getCal().add(Calendar.DATE, nb_j);
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "p=" + p +
                ", d=" + d.toString() +
                ", nb=" + nb +
                ", nb_j=" + nb_j +
                ", emp=" + emp +
                ", end=" + end.toString() +
                '}';
    }

    // Getters
    public My_Date getEnd() {
        return end;
    }
    public Emplacement getEmp() {
        return emp;
    }
    public Personne getP() {
        return p;
    }
    public int getNb() {
        return nb;
    }
    public int getNb_j() {
        return nb_j;
    }
    public My_Date getD() {
        return d;
    }

    // Setters
    public void setNb(int nb) {
        this.nb = nb;
    }
    public void setP(Personne p) {
        this.p = p;
    }
    public void setD(My_Date d) {
        this.d = d;
    }
    public void setNb_j(int nb) {
        this.end.getCal().add(Calendar.DATE, nb - nb_j);
        this.nb_j = nb;
    }
    public void setEmp(Emplacement emp) {
        this.emp = emp;
    }
    public void setEnd(My_Date end) {
        this.end = end;
    }


    public double calPrix() {
        double prix = 0.0;

        if (d.isBefore(new My_Date(1, 7, d.getCal().get(Calendar.YEAR))) &&
                !d.isBefore(new My_Date(31, 8, d.getCal().get(Calendar.YEAR)))) {
            prix = prix_p_hiver * nb;
            if (this.emp.getType().equals("bungalow") || this.emp.getType().equals("Bungalow"))
                prix += prix_u_bung_hiver * nb_j;
            else
                prix += prix_u_autre_hiver * nb_j;
        } else {
            prix = prix_p_ete * nb;
            if (this.emp.getType().equals("bungalow") || this.emp.getType().equals("Bungalow"))
                prix += prix_u_bung_ete * nb_j;
            else
                prix += prix_u_autre_ete * nb_j;
        }
        return prix;
    }
}
