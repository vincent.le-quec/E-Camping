import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

// /!\ WARNING! Month starts from 0!!! /!\

public class My_Date implements Serializable {
    private static final long serialVersionUID = 84L;

    Calendar cal;

    //Date acutelle
    public My_Date() {
        this.cal = new GregorianCalendar();
    }

    //Date à entrer
    public My_Date(int day, int month, int year) {
        this.cal = new GregorianCalendar(year, month - 1, day);
    }

    public String toString() {
        return cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR);
    }

    public void aff() {
        System.out.println(this.toString());
    }

    public boolean isBefore(My_Date d) {
        return (this.cal.before(d.getCal()));
    }

    public Calendar getCal() {
        return cal;
    }
}
