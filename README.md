# E-Camping


## Authors:
* **Vincent LE QUEC** - [Venatum](https://gitlab.com/vincent.le-quec)
* **Corentin BOSQUET** - [CorentinB](https://gitlab.com/CorentinB)
* **Quentin GALLIOU** - [kentiindu29](https://gitlab.com/kentiindu29)

## License
Copyright © 2017, [Vincent Le Quec](https://gitlab.com/vincent.le-quec) & [Corentin Bosquet](https://gitlab.com/CorentinB) & [Quentin Galliou](https://gitlab.com/kentiindu29)
Contact us if you want to use this project and we will authorize you.
